<?php

namespace App\Form;

use App\Entity\Designation;
use App\Entity\Profile;
use App\Entity\Users;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsersFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('address', ProfileFormType::class)
            ->add('name')
            ->add('username')
            ->add('email')
            ->add('mobile')
            ->add('password')
            ->add('profile_picture', FileType::class)
            ->add('designation', EntityType::class,[
                'class' => Designation::class,
                'placeholder' => '--- select a designation ---'
            ])
            ->add('profile', ProfileFormType::class,[
                'data_class' => Profile::class,
                'label' => false
            ])
            ->add('submit', SubmitType::class)
        ;

//        $builder
//            ->add('profile', CollectionType::class,[
//                'entry_type' => ProfileFormType::class,
//                'entry_options' => [
//                    'label' => false
//                ]
//            ])
//            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Users::class,
        ]);
    }
}
