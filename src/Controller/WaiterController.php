<?php

namespace App\Controller;

use App\Entity\Waiter;
use App\Form\WaiterFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/waiter", name="waiter_")
 * Class WaiterController
 * @package App\Controller
 */
class WaiterController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        return new Response('Waiter List');
    }

    /**
     * @Route("/add", name="add")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function addWaiter(Request $request, EntityManagerInterface $entityManager): Response
    {
        $waiter = new Waiter();
        $form = $this->createForm(WaiterFormType::class, $waiter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $entityManager->persist($waiter);
            $entityManager->flush();

            $this->addFlash('successWaiterAdd', 'Waiter added Successfully');

            return $this->redirectToRoute('waiter_index');
        }

        return $this->render('waiter/add.html.twig',[
            'form' => $form->createView()
        ]);
    }
}
