<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/product", name="product_")
 * Class ProductController
 * @package App\Controller
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        return $this->render('product/index.html.twig');
    }

    /**
     * @Route("/add", name="add")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function addProduct(Request $request, EntityManagerInterface $entityManager): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductFormType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $productImage = $request->files->get('product_form')['image'];

            if ($productImage){
                $fileName = md5(uniqid()) . '.' . $productImage->getClientOriginalExtension();
                $productImage->move(
                    $this->getParameter('uploads_dir') . '/product',
                    $fileName
                );

                $product->setImage($fileName);

                $entityManager->persist($product);
                $entityManager->flush();

                $this->addFlash('successProductMessage', 'Product added successfully');
            }

            return $this->redirectToRoute('product_index');
        }

        return $this->render('/product/add.html.twig',[
            'form'=>$form->createView()
        ]);

    }
}
