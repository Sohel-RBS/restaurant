<?php

namespace App\Controller;

use App\Entity\Profile;
use App\Entity\Users;
use App\Form\ProfileFormType;
use App\Form\UsersFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserController extends AbstractController
{
    /**
     * @Route("/", name="register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $user = new Users();

        $form = $this->createForm(UsersFormType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $entityManager = $this->getDoctrine()->getManager();

            /**
             * @var UploadedFile $file
             */

//            $file = $request->files->get('student')['photoName'];
            $file = $request->files->get('users_form')['profile_picture'];

//            $pass = $request->get('users_form')['password'];
//            $encoded = $encoder->encodePassword($user, $pass);
//            $user->setPassword($encoded);

            if($file){
                $filename = md5(uniqid()) . '.' . $file->getClientOriginalExtension();
                $file->move($this->getParameter('uploads_dir') . '/user', $filename);

                $user->setProfilePicture($filename);
                $entityManager->persist($user);
                $entityManager->flush();
                $this->addFlash('success', 'User added successfully');
            }
            return $this->redirectToRoute('register');
        }
        return $this->render('user/register.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/login", name="login")
     */
    public function login()
    {
        return $this->render('user/login.html.twig');
    }
}
